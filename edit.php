<?php
// include database connection file
include_once("config.php");
 
// Check if form is submitted for user update, then redirect to homepage after update
if(isset($_POST['update']))
{	
	$id = $_POST['id'];
	
	$Nama=$_POST['Nama'];
	$Kelas=$_POST['Kelas'];
	$Nomor=$_POST['Nomor'];
	$Hobi=$_POST['Hobi'];
		
	// update user data
	$result = mysqli_query($mysqli, "UPDATE users SET Nama='$Nama',Kelas='$Kelas',Nomor='$Nomor',Hobi='$Hobi' WHERE id=$id");
	
	// Redirect to homepage to display updated user in list
	header("Location: index.php");
}
?>
<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];
 
// Fetech user data based on id
$result = mysqli_query($mysqli, "SELECT * FROM users WHERE id=$id");
 
while($user_data = mysqli_fetch_array($result))
{
	$Nama = $user_data['Nama'];
	$Kelas = $user_data['Kelas'];
	$Nomor = $user_data['Nomor'];
	$Hobi = $user_data['Hobi'];
}
?>
<html>
<head>	
	<title>Edit User Data</title>
</head>
 
<body>
	<a href="index.php">Home</a>
	<br/><br/>
	
	<form name="update_user" method="post" action="edit.php">
		<table border="0">
			<tr> 
				<td>Nama</td>
				<td><input type="text" name="Nama" value=<?php echo $Nama;?>></td>
			</tr>
			<tr> 
				<td>Kelas</td>
				<td><input type="text" name="Kelas" value=<?php echo $Kelas;?>></td>
			</tr>
			<tr> 
				<td>Nomor</td>
				<td><input type="text" name="Nomor" value=<?php echo $Nomor;?>></td>
			</tr>
			<tr> 
				<td>Hobi</td>
				<td><input type="text" name="Hobi" value=<?php echo $Hobi;?>></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>